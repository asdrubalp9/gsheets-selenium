
from env_file import *
from general_functions import *
from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
###################################Librerias random
import time
import datetime
import random
import re
from time import sleep
import json
import requests
#from bs4 import BeautifulSoup
######################################coneccion con BD
import pymysql
import pymysql.cursors


###funcion para random sleep cuando devmode es false
devMode = 0
#botToken = '1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4'
#chat_id = '929814963'


########
start_time = time.time()
if MODO_DEV == 1:
    api_url = 'http://850a2a4a2dbd.ngrok.io'
    api_url = 'http://localhost:89'
    api_url = 'https://mlscanner-dev.invertronica.cl' 

if MODO_DEV == 0:
    api_url = 'https://mlscanner.invertronica.cl' 

req = requests.get(api_url+'/api/tiendas_scan_list')
tienda_id = 1

if req.status_code == 200:
    it = 0
    i = 0
    tiendas = req.json()
    start_time = time.time()
    total_tiendas = len(tiendas)
    ultima_tienda = ''
    print('JSONNNNNNNNNN',req.json() )
    #while it <= len(req.json()):
    chrome_options = webdriver.ChromeOptions()
    prefs = {'profile.default_content_setting_values': {'cookies': 2, 'images': 2, 
                            'plugins': 2, 'popups': 2, 'geolocation': 2, 
                            'notifications': 2, 'auto_select_certificate': 2, 'fullscreen': 2, 
                            'mouselock': 2, 'mixed_script': 2, 'media_stream': 2, 
                            'media_stream_mic': 2, 'media_stream_camera': 2, 'protocol_handlers': 2, 
                            'ppapi_broker': 2, 'automatic_downloads': 2, 'midi_sysex': 2, 
                            'push_messaging': 2, 'ssl_cert_decisions': 2, 'metro_switch_to_desktop': 2, 
                            'protected_media_identifier': 2, 'app_banner': 2, 'site_engagement': 2, 
                            'durable_storage': 2}}
    
    
    chrome_options.add_experimental_option("prefs", prefs)
    if CHROME_VISIBLE == 0 :
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--headless')
    chrome_options.add_argument("--width=2560")
    chrome_options.add_argument("--height=1440")
    chrome_options.add_argument("--window-size=1280,1696")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-extensions")
    driver = webdriver.Chrome(executable_path=r".\extras\chromedriver.exe",chrome_options=chrome_options)
    driver.set_window_position(0, 0)
    driver.set_window_size(1920, 1080)
    sendTeleMessage('Empezando scan')
    for tienda in req.json():
        if tienda['end'] is not None:
            print('Ya se escaneo la tienda', tienda['nombre_tienda'])
            continue
        print("-------------------------------------------\n ")
        url_tienda = tienda['url'] #link
        url_tienda = url_tienda + '_DisplayType_LF_'
        tienda_id = tienda['tienda_id']
        enlace_id = tienda['enlace_id']
        tienda_nombre = tienda['nombre_tienda']
        print("link: ",url_tienda)
        print("Tienda: ",tienda_nombre)
        print("keywords: ",tienda['keywords'])
        if tienda['keywords'] is None:
            keywords = []
        else:
            keywords = tienda['keywords'].split(', ')
        keywords = [sub.upper() for sub in keywords]
        print("keywords: ",keywords)
        it += it + 1
        driver.get(url_tienda)
        wait = WebDriverWait(driver, 15)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        try:
            wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".andes-pagination")))
            paginacion = driver.find_elements_by_class_name("andes-pagination__button")
        except:
            paginacion = [1]
            print('no tiene paginacion')
        print('paginacion---> ', len(paginacion))
        if ultima_tienda != tienda_id: 
            ultima_tienda = tienda_id
            ini_json = { "tienda_id": tienda_id, "status" : "ini" } 
            print('---------------------> Iniciando scan de tienda: %s, json: %s' %( str(tienda_nombre), str(ini_json) ) )
            sendTeleMessage('Iniciando scan: %s ,tienda_id: %s, total tiendas: %s ' %( str(tienda_nombre), str(tienda['tienda_id']),str(total_tiendas) ) )
            post_request = requests.post( api_url + '/api/proceso_scan' , json = ini_json)
            print('resultado', post_request.json() )
        #for paginas in paginacion:
        while True:
            
            randomSleep(devMode)
            elementos = driver.find_elements_by_class_name("ui-search-layout__item")
            iteracion_items = 0
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            lista_productos = []
            HacerlaCorta = 0

            for items in elementos:
                try:
                    if MODO_DEV == 1:
                        HacerlaCorta += 1
                        if HacerlaCorta >= 3 :
                            break
                    #try: 
                    randomSleep(devMode)
                    title = items.find_element_by_class_name("ui-search-item__title").text
                    link = items.find_element_by_class_name("ui-search-link").get_attribute('href')
                    print('item------>%s de %s, %s, item: %s -> %s' %(str(tienda['tienda_id']),str(total_tiendas),str(tienda_nombre),str(iteracion_items), str(title) ) )
                    link = link.upper()
                    link = link.split('#')
                    link = link[0]
                    link = link.split('?')
                    link = link[0]
                    title = title.replace("'",'')
                    title = title.replace('"','')
                    driver.execute_script("window.open('%s'),'new window'"%(link))
                    driver.switch_to_window(driver.window_handles[1])
                    driver.save_screenshot("screenshot.png")
                    try:
                        cantidad = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".ui-pdp-subtitle"))).text
                        if "vendidos" in cantidad.lower():
                            patron_numerico = '\d+'
                            cantidad = re.findall(patron_numerico, cantidad)
                            cantidad = cantidad[0]
                        else:
                            cantidad = 0
                    except:
                        cantidad = 0
                        driver.close()
                        driver.switch_to_window(driver.window_handles[0])
                        continue
                    # cantidad = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".ui-pdp-subtitle"))).text
                    # pattern = '\d+'
                    # cantidad = re.findall(pattern, cantidad) 
                    # if len(cantidad) > 0:
                    #     cantidad = cantidad[0]
                    # else:
                    #     cantidad = 0
                    #     driver.close()
                    #     driver.switch_to_window(driver.window_handles[0])
                    #     continue
                    rightSidebar = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".ui-pdp-container--column-right .ui-pdp-price__second-line .price-tag .price-tag-fraction")))
                    precio = rightSidebar.text
                    print('TITULO ',title)
                    print('Link ',link)
                    print('Precio ',precio)
                    print('cantidad ',cantidad)
                    metaData = obtenerMetaData(driver)
                    datos = {'titulo':title,'precio':precio, 'cantidad':cantidad, 'link':link, 'atributos' : metaData}
                    print('++++++++++++++++++++++++++++++++++++++')
                    #except: 
                    lista_productos.append(datos)
                    print('datos',datos)
                    driver.close()
                    driver.switch_to_window(driver.window_handles[0])

                except Exception as error:
                    driver.save_screenshot("screenshot.png")
                    sendTeleMessage('error: %s, %s' %(str(error), str(link)    ))
                    sendTelegramImage()
                    datos = {'titulo':'1','precio':'1', 'cantidad':'1', 'link':'1'}
                    driver.close()
                    driver.switch_to_window(driver.window_handles[0])
                
            try:
                boton = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".andes-pagination__button.andes-pagination__button--next")))
            except:
                print('No hay mas paginas que escanear')
                break

            datos = {'tienda' : { 'tienda_id': tienda['tienda_id'],  'productos' : lista_productos  } }
            print('datos',datos)
            post_request = requests.post(api_url+'/api/process_scan_data', data = json.dumps(datos) )
            print('--------------> SENDING DATA TO SERVER Codigo: %s' %( str(post_request.status_code)  ) )
            if post_request.status_code == 200:
                print('--------------> SENDING DATA TO SERVER mensaje: %s' %(  str( post_request.json() ) ) )
            print("-----------FIN PAGINA-----------")
            # try:
            # except Exception as e:
            #     print('---------------> ERROR al mandar data al servidor', e)
            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            try:
                boton = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".andes-pagination__button.andes-pagination__button--next")))
                boton.click()
            except:
                print("No se consiguio el boton")
                #brea    
        #post_request = requests.post(api_url+'/api/proceso_scan' ,data = json.dumps(datos))
        post_request = requests.post(api_url+'/api/proceso_scan' ,json = { "tienda_id": tienda_id, "status" : "fin" } )
        sendTeleMessage('Fin de scan: %s ,tienda_id: %s, total tiendas: %s ' %( str(tienda_nombre), str(tienda['tienda_id']),str(total_tiendas) ) )

        print('--------------> REQUEST TO API, CLOSING', post_request.json() )
    if driver : 
        driver.close()
else:
    print('sin conexion al servidor')  
print("El scan termino despues de '%s' hrs"% (  str( ((time.time() - start_time )/60)/60 )) )
sendTeleMessage("El scan termino despues de '%s' hrs"% (  str( ((time.time() - start_time )/60)/60 )))

exit()