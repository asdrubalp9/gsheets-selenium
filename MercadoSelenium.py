from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as E
from selenium.webdriver.common.action_chains import ActionChains
###################################Librerias random
import time
import datetime
import random
import re
from time import sleep
######################################coneccion con BD
import pymysql
import pymysql.cursors


conn = pymysql.connect(host='localhost', user="root", password="", db="mercadohitler" )
cursor = conn.cursor()
##########################################
#################### LINKS

# https://listado.mercadolibre.cl/_CustId_246097911
# https://listado.mercadolibre.cl/_CustId_133976842
# https://listado.mercadolibre.cl/_OrderId_PRICE_CustId_133976842_PriceRange_7000-0_seller*id_133976842
##################


###funcion para random sleep cuando devmode es false
devMode = 0
def randomSleep(active):
    if active == 0:
        tiempo = random.randint(0,8)
        print("durmiendo %s" %(tiempo))
        sleep(tiempo)
########
        
list = [
    "https://listado.mercadolibre.cl/fernapet",
    "https://listado.mercadolibre.cl/_CustId_246097911",
    "https://listado.mercadolibre.cl/_CustId_133976842",
    "https://listado.mercadolibre.cl/_OrderId_PRICE_CustId_133976842_PriceRange_7000-0_seller*id_133976842",
    "https://listado.mercadolibre.cl/_CustId_280158028",
    "https://listado.mercadolibre.cl/_CustId_32019805",
    "https://listado.mercadolibre.cl/_CustId_237892006",
    "https://listado.mercadolibre.cl/_CustId_216918630",
    "https://listado.mercadolibre.cl/_CustId_246097911",
    "https://eshops.mercadolibre.cl/todoventas+chile",
    "https://listado.mercadolibre.cl/moza-importaciones",
    "https://listado.mercadolibre.cl/_CustId_168268179",
    "https://listado.mercadolibre.cl/_CustId_264602405",
    "https://listado.mercadolibre.cl/cupoclick",
    "https://listado.mercadolibre.cl/_CustId_182068874",
    "https://listado.mercadolibre.cl/_CustId_237204860",
    "https://listado.mercadolibre.cl/_CustId_133976842",
    "https://listado.mercadolibre.cl/_CustId_258161576",
    "https://www.mercadolibre.cl/perfil/DETAILSTORE2018",
    "https://www.mercadolibre.cl/perfil/ESHOPANGIE",
    "https://listado.mercadolibre.cl/_CustId_320077703",
    "https://listado.mercadolibre.cl/_CustId_370234831",
    "https://listado.mercadolibre.cl/_CustId_217238942",
    "https://listado.mercadolibre.cl/_CustId_247241136",
    "https://listado.mercadolibre.cl/_CustId_216244489",
    "https://eshops.mercadolibre.cl/requete+ofertas",
    "https://listado.mercadolibre.cl/_CustId_231655899",
]

driver = webdriver.Chrome(executable_path=r"D:\bots\scrappers\chromedriver")

it = 0
i=0
while it < len(list):
    print("-------------------------------------------\n ")
    a = list[it] #link
    print(a)
    print(it)
    driver.get(a)
    elemento = driver.find_elements_by_class_name("results-item")
    paginacion = driver.find_elements_by_class_name("andes-pagination")
    it = it + 1 ##iteraciones para un loop
    for paginas in paginacion:        
        randomSleep(devMode)
        for items in elemento:
            break
            randomSleep(devMode)
            fechaScrape = datetime.datetime.now()
            #element = driver.find_element_by_class_name("results-item")
            title = items.find_element_by_class_name("item__title").text
            link = items.find_element_by_class_name("item__info-title").get_attribute('href')
            productId = items.find_element_by_class_name("item__info-title").get_attribute('id')
            string = items.find_element_by_class_name("item__condition").text
            precio = items.find_element_by_class_name("price__fraction").text
            print(title)
            print(link)
            pattern = '\d+'
            cantidad = re.findall(pattern, string) 
            print(cantidad[0]) 
            print(productId)
            print(precio)
            query = "SELECT id FROM productos WHERE link = '%s'" %(link)
            cursor.execute(query)
            linkProducto = cursor.fetchone()                
            if linkProducto == None:
                query = "INSERT INTO `productos`( `nombreProducto`, `link`, `precio`,`fechaScrape`, `idProducto` ) VALUES ('%s','%s','%s','%s','%s');"
                print(query)
                result = cursor.execute(query,(str(title), str(link), str(precio),str(fechaScrape),str(productId)))
                lastId = cursor.lastrowid
                print("UltimoGuardado: '%s'"%(lastId))
                conn.commit()
                ######
            else:
                lastId = linkProducto[0]
            print(lastId)
            query = "INSERT INTO `cantidades` (`idProducto`, `cantidad`) VALUES('%s','%s');"%(str(lastId) ,str(cantidad[0]))
            print(query)
            result = cursor.execute(query)
            conn.commit()
            if result:
                print("Se guardo la cantidad '%s'"%cantidad[0])
            else:
                print("error al guardar la cantidad")
                
        ##    driver.implicitly_wait(3)
            driver.execute_script("window.open('%s'),'new window'"%(link))
            
            driver.switch_to_window(driver.window_handles[1])
            
            try:
                tendencia = driver.find_elements_by_class_name("questions__group")
                cantidadDeClics = 0
                while cantidadDeClics <= 2:
                    randomSleep(devMode)
                    driver.find_element_by_id('showMoreQuestionsButton').click()
                    cantidadDeClics = cantidadDeClics + 1
            except:
                print("no se hizo clic para buscar preguntas")
            ###
            query = "SELECT fecha FROM tendencia WHERE idProducto = '%s' ORDER BY CONVERT(fecha,DATETIME) DESC LIMIT 1" 
            print(query)
            cursor.execute(query,(lastId))
            ultimaFecha = cursor.fetchone()
            ###
            for subItems in tendencia:
                print("-------------------------------------------\n ")
                #driver.execute_script("window.scrollTo(0, document.body.scrollHeight);") ##scroll to bottom
                ActionChains(driver).move_to_element(driver.find_element_by_id('showMoreQuestionsButton')).perform()
                driver.implicitly_wait(0)
                pregunta = subItems.find_element_by_class_name("questions__content").text
                try:
                    fecha = subItems.find_element_by_class_name("questions__time").text
                except:
                    pass
                #print("fecha %s ultimaFecha %s" %(fecha, ultimaFecha[0]))
                if not ultimaFecha:
                    pass
                else:
                    if ultimaFecha[0] == fecha: 
                        break
                        print("no hay mas preguntas")
                    
                query = "INSERT INTO `tendencia`(`idProducto`, `fecha`, `lastQuestion`) VALUES ('%s','%s','%s');"
                print(query)
                if fecha != '':
                    if query != '':
                        try:
                            guardarPreguntas = cursor.execute(query, ( str(lastId), str(fecha.strip()), str(pregunta.trip()) ))
                            conn.commit()
                        except:
                            print("no se pudo guardar el query: %s" %(query))
                        if guardarPreguntas == 1:
                            print("se guardo la pregunta")
                                
            nombreTienda = driver.find_element_by_class_name("card-block-link").get_attribute('href')
            print(nombreTienda.replace("https://perfil.mercadolibre.cl/",""))
            try :
                query = "UPDATE `productos` SET `nombreTienda`= '%s' WHERE id ='%s'"%(nombreTienda.replace("https://perfil.mercadolibre.cl/",""), lastId)
                cursor.execute(query)
                conn.commit()
                print("se actualizo la tienda")
            except:
                print("NO se actualizo la tienda")
            driver.close()
            driver.switch_to_window(driver.window_handles[0])
            
            print("-------------------------------------------\n ")
        driver.find_element_by_class_name('andes-pagination__button--next').click()
    

    ##lastId = "SELECT id, link FROM productos order by id DESC LIMIT 1"
    ##cursor.execute(query)
    ##lastId = cursor.fetchone()
    ##cursor.close()
    ##print("lastId '%s'"%(result))

        
    ##driver.close()

    ##driver.quit()
    ####
