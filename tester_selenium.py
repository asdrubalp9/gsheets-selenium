from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
###################################Librerias random
import time
import datetime
import random
import re
from time import sleep
import requests
import json
#from bs4 import BeautifulSoup
######################################coneccion con BD
import pymysql
import pymysql.cursors

def sendImage():
    botToken = '1003788364:AAEQRO-YOKBTbQXN3Ilj38G9cSKj4aU54L4'
    chat_id = '929814963'
    url = "https://api.telegram.org/bot"+botToken+"/sendPhoto"
    files = {'photo': open('screenshot.png', 'rb')}
    data = {'chat_id' : chat_id}
    r= requests.post(url, files=files, data=data)
    print(r.status_code, r.reason, r.content)


def obtenerMetaData(driver):
    wait = WebDriverWait(driver, 4)
    print('obtenerMetaData')
    try:
        wait.until( EC.presence_of_element_located((By.XPATH, '//*[text()="Características generales"]//ancestor::table//child::tbody//child::tr' )))
        filas = driver.find_elements(By.XPATH, '//*[text()="Características generales"]//ancestor::table//child::tbody//child::tr')
        print('->filas', filas)
        atributos = []
        for row in filas:
            th = row.find_element_by_css_selector('th').text 
            td = row.find_element_by_css_selector('td').text 
            print( 'row->%s, %s,' %(str(th), str(td)) )
            atributos.append({th:td})
        return atributos
    except:
        print('item no tiene caracteristicas generales')
        return 0

        


chrome_options = webdriver.ChromeOptions()
prefs = {"profile.managed_default_content_settings.images": 2}
prefs = {'profile.default_content_setting_values': {'cookies': 2, 'images': 2, 
                    'plugins': 2, 'popups': 2, 'geolocation': 2, 
                    'notifications': 2, 'auto_select_certificate': 2, 'fullscreen': 2, 
                    'mouselock': 2, 'mixed_script': 2, 'media_stream': 2, 
                    'media_stream_mic': 2, 'media_stream_camera': 2, 'protocol_handlers': 2, 
                    'ppapi_broker': 2, 'automatic_downloads': 2, 'midi_sysex': 2, 
                    'push_messaging': 2, 'ssl_cert_decisions': 2, 'metro_switch_to_desktop': 2, 
                    'protected_media_identifier': 2, 'app_banner': 2, 'site_engagement': 2, 
                    'durable_storage': 2}}
chrome_options.add_experimental_option("prefs", prefs)
#chrome_options.add_argument('--headless')
chrome_options.add_argument("--disable-infobars")
chrome_options.add_argument("--disable-extensions")
driver = webdriver.Chrome(executable_path=r".\extras\chromedriver.exe",chrome_options=chrome_options)
#driver.maximize_window()
#driver.get('HTTPS://ARTICULO.MERCADOLIBRE.COM.PE/MPE-436417961-ALIMENTO-MIOCANE-PARA-PERROS-CACHORRO-ADULTO-PIELSEMSIBLE-_JM')
driver.get('https://articulo.mercadolibre.com.pe/MPE-440511849-set-20-kg-pesas-cromadas-convertible-importadas-marca-york-_JM')
wait = WebDriverWait(driver, 15)
atributos = obtenerMetaData(driver)
try:
    cantidad = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".ui-pdp-subtitle"))).text
    print('CANTIDAD',cantidad)
    print('->',"vendidos" in cantidad.lower() )
    if "vendidos" in cantidad.lower():
        patron_numerico = '\d+'
        cantidad = re.findall(patron_numerico, cantidad)
        cantidad = cantidad[0]
    else:
        cantidad = 0
except:
    #continue
    cantidad = 0
    

print('La cantidad es: %s'%( str(cantidad) ))
    


driver.close()
driver.switch_to_window(driver.window_handles[0])
        

##print('---> atributos: ',atributos)
driver.close()

