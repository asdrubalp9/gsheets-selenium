import os
import re
from Google import Create_Service
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pprint import pprint


CLIENT_SECRET_FILE = 'credentials.json'
API_NAME = 'sheets'
API_VERSION = 'v4'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

spreadsheet_id = '1JE-9Z0SRX88QYaRbCT1aOM9VBTB0SiZoGeD8CVvPA-M'
service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

"""
Obtener todas las tiendas
"""
list_tiendas = service.spreadsheets().values().get(
    spreadsheetId = spreadsheet_id,
    majorDimension = 'COLUMNS',
    range = 'ListaTiendas!A2:B'
).execute()

mySpreadsheet = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()
#print(mySpreadsheet['sheets'])


#print(list_tiendas['values'])
"""
    leer sheets
"""
sheet_list = []

for sheet in mySpreadsheet['sheets']: 
    sheet_list.append(sheet['properties']['title'])

sheet_index = 1
for tiendas in list_tiendas['values']:
    #print('tiendas',len(tiendas))
    for tienda in tiendas:
        try: 
            if 'https://' not in tienda:
                default_sheets = ['ListaTiendas', 'plantilla']
                if tienda not in sheet_list:
                    sheet_index += 1
                    request_body = {
                        'requests' : [
                            {
                                'duplicateSheet': {
                                    'sourceSheetId': '53307381',
                                    'newSheetName' : tienda,
                                    'insertSheetIndex':sheet_index,
                                }
                            }
                        ]
                    }
                    duplication = service.spreadsheets().batchUpdate(
                        spreadsheetId = spreadsheet_id,
                        body = request_body
                    ).execute()
                    print('duplication',duplication)
                    
                    #limpiar celdas                    
                    # request_body =  {
                    #     'request' : [
                    #         {
                    #             'ranges': [
                    #                 'A3:AA'
                    #             ],  
                    #             'updateCells' : {
                    #                 'range':{
                    #                     'sheetId': duplication['replies'][0]['duplicateSheet']['properties']['sheetId']
                    #                 },
                    #                 'fields': 'userEnteredValue'
                    #             },
                    #         }
                    #     ],
                        

                        
                    # }
                    
                    # limpieza = service.spreadsheets().values().batchClear(
                    #     spreadsheetId = spreadsheet_id,
                    #     body = request_body,
                    # )
                    # print('limpieza'+limpieza)
                    

        except Exception as e:
            print(e)

list_tiendas = service.spreadsheets().values().get(
    spreadsheetId = spreadsheet_id,
    majorDimension = 'COLUMNS',
    range = 'ListaTiendas!A2:D'
).execute()
"""
chrome_options = webdriver.ChromeOptions()
#prefs = { "profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option( "prefs" , { 
    "profile.default_content_setting_values.notifications" : 2,
    'credentials_enable_service': False,
    'profile': {
        'password_manager_enabled': False
    }
})
"""
chrome_options = Options()
chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--enable-new-usb-backend")
chrome_options.add_argument('--disable-extensions')
chrome_options.add_argument("user-agent=Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0")
chrome_options.add_experimental_option("excludeSwitches", ['enable-automation'])
#, options=chrome_options
#driver = webdriver.Chrome(executable_path=r'.\extras\chromedriver87.exe', options = chrome_options)
driver = webdriver.Chrome(executable_path=r'.\extras\chromedriver.exe', options = chrome_options)
wait = WebDriverWait(driver, 15)
print('-------->list_tiendas')
pprint(list_tiendas['values'])
for tienda, url,words in zip(list_tiendas['values'][0], list_tiendas['values'][1],list_tiendas['values'][3]):
    print('----> tienda',tienda)
    print('----> url',url)
    keywords = words.split(', ')
    #print('-->keywords')
    #pprint(keywords)
    if 'https:/' in url:
        driver.get(url)
        elements = driver.find_elements_by_class_name('ui-search-result__content-wrapper')
        #rows = wait.until(EC.element_to_be_clickable((By.CLASS_NAME,'ui-search-result__content-wrapper')))
        
        for row in elements:
            print('row->',row)
            titulo = row.find_element_by_css_selector('.ui-search-item__group__element.ui-search-link')
            print('--->T ',titulo.text)
            titulo = titulo.text
            link_producto = row.find_element_by_css_selector('.ui-search-item__group__element.ui-search-link').get_attribute("href")
            print('--->L ',link_producto)
            link_producto = link_producto
            print(link_producto , titulo )
            
            open_page = 0
            print('num keywords', len(keywords))
            if len(keywords) > 0:
                print('si hay keywords')

                for keyword in keywords:
                    if keyword in titulo:
                        open_page = 1
                        print('la palabra '+ keyword+ 'Se encuentra en '+titulo)
                        driver.execute_script("window.open('');")
                        driver.switch_to.window(driver.window_handles[1])
                        driver.get(link_producto)
                        cantidad = driver.find_element_by_css_selector('.ui-pdp-subtitle').text
                        cantidad = cantidad.replace("|","")
                        cantidad = cantidad.replace(" ","")
                        cantidad = cantidad.replace("vendidos","")
                        cantidad = cantidad.replace("Nuevo","")
                        print('====> cantidad', cantidad)
                        driver.close()
                        driver.switch_to.window(driver.window_handles[0])
                    else:
                        print('no matches')
            else:
                open_page = 1
                print('no hay keywords')
        

        try:
            
            elWait = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, "andes-pagination__button")))
            print('elWait',elWait)
            elements = driver.find_element( By.CSS_SELECTOR , ".ui-search-results a.ui-search-link.ui-search-item__group__element" )

            print('---->ELEMENTOS ', elements, len(elements))
            
            # for element in elements:
            #     print('-->', element)

        except Exception as e:
            print(e)    
        finally:
            print('fin coso')
            #driver.quit()

#driver.quit()




        


##buscar informacion de las tiendas //scrapper time
exit 

#mySpreadsheet.keys()
#for sheet in mySpreadsheet['sheets']:
    #print(sheet['properties'])


"""
    end leer sheets


##########
worksheet_name = 'plantilla!'
cell_range_insert = 'J3'
values = (
    ('asd','asd','asd'),
    ('qwe','qwe','qwe'),
    
)

value_range_body = {
    'majorDimension' : 'ROWS',
    'values' : values
}

service.spreadsheets().values().append(
    spreadsheetId = spreadsheet_id,
    valueInputOption = 'RAW',
    range = worksheet_name + cell_range_insert,
    body = value_range_body
).execute()

"""