from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import string

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
spreadsheet_id = '1JE-9Z0SRX88QYaRbCT1aOM9VBTB0SiZoGeD8CVvPA-M'


def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    fila_modelos = 'plantilla!A2:AA'
    sheet = service.spreadsheets()  

    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, range=fila_modelos ).execute()
    rows = result.get('values', [])
    #print(rows)

################################################################

    Spreadsheets = service.spreadsheets().get(spreadsheetId = spreadsheet_id).execute()
    print(Spreadsheets)

    worksheet_name = 'plantilla!'
    cell_range_insert = 'J3'
    
    values = (
        ('colA', 'colB','colC'),
        ('appel', 'orange','watermelon'),
    )

    value_range_body = {
        'majorDimension': 'ROWS',
        'values':values
    }

    service.spreadsheets().values().append(
        spreadsheetId = spreadsheet_id,
        valueInputOption = 'USER_ENTERED',
        range = worksheet_name + cell_range_insert,
        body = value_range_body
    ).execute()


    
        

if __name__ == '__main__':
    print(string.ascii_lowercase)
    main()