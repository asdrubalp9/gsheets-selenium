from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
###################################Librerias random
import time
import datetime
import random
import re
from time import sleep
######################################coneccion con BD
import pymysql
import pymysql.cursors


conn = pymysql.connect(host='localhost', user="root", password="", db="mercadolibre_seguimiento" )
#conn = pymysql.connect(host='localhost', user="root", password="", db="mercadolibre_seguimiento_dev" )
cursor = conn.cursor()

###funcion para random sleep cuando devmode es false
devMode = 0
def randomSleep(active):
    if active == 0:
        tiempo = random.randint(0,2)
        print("durmiendo %s" %(tiempo))
        sleep(tiempo)
########

query = "SELECT * FROM tiendas WHERE seguir = 1 AND deleted_at IS NULL order by ID ASC "
cursor.execute(query)
tiendas = cursor.fetchall()
total_tiendas = len(tiendas)
print('Tiendas sacadas de la BD: ', len(tiendas))
print(tiendas)
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.managed_default_content_settings.images": 2}
chrome_options.add_experimental_option("prefs", prefs)
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
driver = webdriver.Chrome(executable_path=r"C:\codigo\bots\mlBot\extras\chromedriver.exe",chrome_options=chrome_options)

it = 0
i=0
start_time = time.time()
while it < len(tiendas):
    print("-------------------------------------------\n ")
    print('TIENDA: ', tiendas[it])
    url_tienda = tiendas[it][2] #link
    url_tienda = url_tienda + '_DisplayType_LF_'
    tienda_id = tiendas[it][0]
    tienda_nombre = tiendas[it][1]
    print("link: ",url_tienda)
    print(it)
    print("Tienda: ",tienda_nombre)
    keywords = tiendas[it][3].split(', ')
    keywords = [sub.upper() for sub in keywords]
    print("keywords: ",keywords)
    driver.get(url_tienda)
    wait = WebDriverWait(driver, 10)
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    try:
        wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".andes-pagination")))
        paginacion = driver.find_elements_by_class_name("andes-pagination__button")
    except:
        paginacion = [1]
        print('no tiene paginacion')
    #paginacion = wait.until( EC.presence_of_all_elements_located(( By.CSS_SELECTOR, ".ui-search-pagination.andes-pagination__button")))
    print('paginacion---> ', len(paginacion))
    it = it + 1 ##iteraciones para un loop
    

    for paginas in paginacion:        
        
        randomSleep(devMode)
        elementos = driver.find_elements_by_class_name("ui-search-layout__item")
        iteracion_items = 0
        for items in elementos:
            try:
                iteracion_items = iteracion_items + 1
                #break
                randomSleep(devMode)
                #element = driver.find_element_by_class_name("results-item")
                title = items.find_element_by_class_name("ui-search-item__title").text
                link = items.find_element_by_class_name("ui-search-link").get_attribute('href')
                #string = items.find_element_by_class_name("item__condition").text
                precio = items.find_element_by_class_name("price-tag-fraction").text
                print('item---------------->%s de %s, %s, item: %s -> %s' %(str(it),str(total_tiendas),str(tienda_nombre),str(iteracion_items), str(title) ) )
                link = link.upper()
                link = link.split('#')
                link = link[0]
                title = title.replace("'",'')
                title = title.replace('"','')
                print('TITULO ',title)
                print('Link ',link)
                print('Precio ',precio)
                query = "SELECT p.id, p.seguir as seguir_producto, s.created_at as fecha FROM productos p LEFT JOIN seguimientos s ON s.producto_id = p.id AND DATE_FORMAT(s.created_at, '%d-%m-%Y') = DATE_FORMAT(NOW(), '%d-%m-%Y') WHERE p.url = '"+link+"'  AND  p.seguir = 1 AND p.tienda_id = "+str(tienda_id)+" ORDER BY s.created_at DESC LIMIT 1" 
                print('QUERYYY ',query)
                cursor.execute(query)
                product_data = cursor.fetchone()
                print("product_data",product_data)
                if product_data == None:
                    query = "INSERT INTO `productos`( `nombre`, `url` , tienda_id ) VALUES ('%s','%s','%s');"%(str(title), str(link), str(tienda_id) )
                    print('---> INSERTANDO -> ',"INSERT INTO `productos`( `nombre`, `url` , tienda_id ) VALUES ('%s','%s','%s');"%(str(title), str(link), str(tienda_id) ) )
                    result = cursor.execute(query)
                    lastId = cursor.lastrowid
                    #print("UltimoGuardado: '%s'"%(lastId))
                    conn.commit()
                    last_scrap_date = 0
                    ######
                else:
                    last_scrap_date = product_data[2]
                    lastId = product_data[0]
                guardar = 0
                #print('last insert fecha---->', last_scrap_date)
                #print('keywords-> ', keywords[0])

                if str(keywords[0]) == str(0):
                    guardar = 1
                    checkIfHasKeyword = True
                else:
                    sub = title.upper()
                    checkIfHasKeyword = any(word in sub for word in keywords)

                if checkIfHasKeyword is True:
                    guardar = 1
                else:
                    guardar = 0
                
                if (guardar == 1) and (last_scrap_date is None) :
                    driver.execute_script("window.open('%s'),'new window'"%(link))
                    driver.switch_to_window(driver.window_handles[1])
                    try:
                        
                        baneado = driver.find_element_by_css_selector("ui-empty-state__container")
                    except:
                        baneado = []
                        print('---------------me banearon!-------------')
                    if len(baneado) > 0:
                        randomSleep(devMode)
                    cantidad = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".ui-pdp-subtitle"))).text
                    pattern = '\d+'
                    cantidad = re.findall(pattern, cantidad) 
                    if len(cantidad) > 0:
                        cantidad = cantidad[0] 
                    else:
                        cantidad = 0
                    print(lastId)
                    query = "INSERT INTO `seguimientos` (`producto_id`, `cantidad`,`precio`, created_at ) VALUES('%s','%s','%s', NOW());"%(str(lastId) ,str(cantidad), str(precio) )
                    print('INSERTANDO ',query)
                    result = cursor.execute(query)
                    conn.commit()
                    driver.close()
                    if result:
                        print("El producto '%s' se ha guardado '%s'"% (  str(title), str(cantidad) ) )
                    else:
                        print("error al guardar la cantidad")
                else:
                    print("El producto no se ha guardado", title) 
                driver.switch_to_window(driver.window_handles[0])
            except:
                driver.close()
                driver.switch_to_window(driver.window_handles[0])
            print("-------------------------------------------\n ")
        #paginacion.find_element_by_class_name()
        #driver.switch_to_window(driver.window_handles[0])
        print("-----------FIN PAGINA-----------")
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        try:
            boton = wait.until( EC.presence_of_element_located(( By.CSS_SELECTOR, ".andes-pagination__button.andes-pagination__button--next")))
            boton.click()
        except:
            print("No se consiguio el boton")
            #brea
print('FIIIIIIIIIIIIIIN!!')
driver.close()
driver.quit()
print("El scan termino despues de '%s' hrs"% (  str( ((time.time() - start_time )/60)/60 )     ) )